import sys
from const import company, manager


@manager.assign('saldo', [float])
def balance(transaction):
    company.account_transaction(transaction)


@manager.assign('sprzedaz', [str, int, float])
def sales(product_name, product_count, product_price):
    company.product_sale(product_name, product_count, product_price)


@manager.assign('zakup', [str, int, float])
def purchase(product_name, product_count, product_price):
    company.product_purchase(product_name, product_count, product_price)


manager.run()


with open(sys.argv[1], 'w') as file:
    for line in company.logs:
        file.write(f"{line}\n")
company.save_logs()
