import sys
from const import company, manager


@manager.assign('saldo', [float])
def balance(transaction):
    company.account_transaction(transaction)


@manager.assign('sprzedaz', [str, int, float])
def sales(product_name, product_count, product_price):
    company.product_sale(product_name, product_count, product_price)


@manager.assign('zakup', [str, int, float])
def purchase(product_name, product_count, product_price):
    company.product_purchase(product_name, product_count, product_price)


manager.run()


company.show_store()
with open(sys.argv[1], 'w') as file:
    for product in range(len(sys.argv) - 2):
        comment = f"{str(sys.argv[product + 2])}: " \
                  f"{str(company.store.get(sys.argv[product + 2])['count'])} " \
                  f"sztuk"
        file.write(f"{comment}\n")
company.save_logs()
