import sys
from const import company, manager


@manager.assign('saldo', [float])
def balance(transaction):
    company.account_transaction(transaction)


@manager.assign('sprzedaz', [str, int, float])
def sales(product_name, product_count, product_price):
    company.product_sale(product_name, product_count, product_price)


@manager.assign('zakup', [str, int, float])
def purchase(product_name, product_count, product_price):
    company.product_purchase(product_name, product_count, product_price)


manager.run()


@manager.assign('sprzedaz', [str, int, float])
def single_action():
    manager.run_single_action('sprzedaz', source='argv')


with open(sys.argv[1], 'w')as file:
    comment = f'({sys.argv[0]}): Sprzedano {sys.argv[2]} - ' \
              f'{sys.argv[4]} szt., {sys.argv[3]} zł/szt.'
    file.write(comment)
company.save_logs()
