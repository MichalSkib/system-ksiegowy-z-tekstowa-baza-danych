import sys


class Company:
    """ Klasa reprezentująca firmę (stan konta i magazyn) """

    def __init__(self):
        self.balance = self.balance_level()
        self.store = self.stock_level()
        self.logs = []  # historia operacji

    def stock_level(self):
        initial_dict = {}
        with open('stock_level.txt') as file_1:
            for line in file_1.readlines():
                divided_line = line.split(';')
                name = divided_line[0]
                count = int(divided_line[1])
                price = int(divided_line[2].replace('\n', ''))
                initial_dict[name] = {
                    'count': count,
                    'price': price
                }
            return initial_dict

    def balance_level(self):
        with open('balance_level.txt') as file_2:
            balance_ = float(file_2.readline())
        return balance_

    def account_transaction(self, money, mode='n'):
        if (money < 0) and (self.balance + money < 0):
            log = f'Nie masz {money} na koncie!'
            self.logs.append(log)
        else:
            self.balance += money
            log = f"Zmiana salda o {money}"
            self.logs.append(log)
            if mode != 'n':
                print(f'{sys.argv[0]} {sys.argv[3]} {sys.argv[2]}')
                print(self.logs)

    def product_purchase(self, name, count, price):
        product_total_price = price * count
        if product_total_price > self.balance:
            print(f'Cena za towary ({product_total_price}) przekracza wartosc'
                  f'salda ({self.balance})')
        else:
            self.balance -= product_total_price
            if not self.store.get(name):
                self.store[name] = {'count': count, 'price': price}
            else:
                store_product_count = self.store[name]['count']
                self.store[name] = {
                    'count': store_product_count + count,
                    'price': price
                }
        log = f'Dokonano zakupu produktu: {name} ' \
              f'w ilosci {count} sztuk, ' \
              f'o cenie jednostkowej {price}.'
        self.logs.append(log)

    def product_sale(self, name, count, price):
        if not self.store.get(name):
            # print('W magazynie nie ma takiego produktu!')
            log = f'W magazynie nie ma takiego produktu!'
            self.logs.append(log)
        if self.store.get(name)['count'] < count:
            # print('Brak wystarczajacej ilosci towaru!')
            log = f'Brak wystarczajacej ilosci towaru!'
            self.logs.append(log)
        self.store[name] = {
            'count': self.store.get(name)['count'] - count,
            'price': price
        }
        self.balance += count * price
        if not self.store.get(name)['count']:
            del self.store[name]
        log = f'Dokonano sprzedazy produktu: {name} ' \
              f'w ilosci {count} sztuk, ' \
              f'o cenie jednostkowej {price}.'
        self.logs.append(log)

    def show_store(self):
        for product in range(len(sys.argv) - 2):
            print('<' + str(sys.argv[product + 2]) + '> : <' +
                  str(self.store.get(sys.argv[product + 2])['count']) + '>')

    def save_logs(self):
        with open('logs.txt', 'a') as file:
            for element in self.logs:
                file.write(self.logs[element])

    def __str__(self):
        return f'{self.logs}'

    def __repr__(self):
        return f'{self.logs}'


class Manager:

    ALLOWED_COMMANDS = ('saldo', 'zakup', 'sprzedaz', 'stop')

    def __init__(self):
        self.actions = {}
        self.counter = 1

    def assign(self, action, args):

        def inner(func):
            self.actions[action] = (func, args)
        return inner

    def read_data(self, label, source):
        if source == 'input':
            return input(label)
        else:
            self.counter += 1
            return sys.argv[self.counter - 1]

    def run_single_action(self, action, source='input'):
        func, args = self.actions[action]
        args2 = []
        for arg in args:
            if arg is str:
                args2.append(self.read_data('Podaj ciag znakow: ', source))
            if arg is float:
                args2.append(float(self.read_data('Podaj liczbe zmiennoprzecinkowa: ', source)))
            if arg is int:
                args2.append(int(self.read_data('Podaj liczbe calowita: ', source)))
        func(*args2)

    def run(self):
        while True:
            action = input('Wpisz komendę: ')
            if action not in self.ALLOWED_COMMANDS:
                print('Niedozwolona komenda!')
                continue
            if action == 'stop':
                print('Koniec programu!')
                break
            self.run_single_action(action)
