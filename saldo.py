import sys
from const import company, manager


@manager.assign('saldo', [float])
def balance(transaction):
    company.account_transaction(transaction)


@manager.assign('sprzedaz', [str, int, float])
def sales(product_name, product_count, product_price):
    company.product_sale(product_name, product_count, product_price)


@manager.assign('zakup', [str, int, float])
def purchase(product_name, product_count, product_price):
    company.product_purchase(product_name, product_count, product_price)


manager.run()


@manager.assign('saldo', [float])
def single_action():
    manager.run_single_action('saldo', source='argv')


transaction = float(sys.argv[2])
company.account_transaction(transaction, mode='t')
with open(sys.argv[1], 'w') as file:
    comment = f"({sys.argv[0]}): {sys.argv[3]} {sys.argv[2]}"
    file.write(comment)
company.save_logs()
